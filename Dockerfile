FROM node:12-alpine
LABEL maintainer="Pedro Vitor C. Pacheco <pedrovcpacheco@yahoo.com>"
WORKDIR /opt
RUN apk add git \
    && git clone  https://gitlab.com/ifpb-es/open-planner/open-planner-frontend.git
WORKDIR /opt/open-planner-frontend
RUN npm i  
ENTRYPOINT ["npm"]
CMD ["start"]
EXPOSE 3000



